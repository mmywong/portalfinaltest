var express = require('express');
var router = express.Router();
var userModel =  require('../models/user');
var dayModel = require('../models/day');
/* GET home page. */
router.get('/', function(req, res, next) {
  var day = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];


  userModel.find({_id: req.params.id}, function(err, docs) {
    res.render('index', { title: 'Express' , _id: req.params.id, day: day, schedule: docs.schedule, comment: docs.comment});
    console.log(docs);
  });
});

router.post('/', function(req, res, next) {
  var sched = [];
  for(var i = 0; i < 5; i++) {
    sched.push(new dayModel({availability: req.body['matrix['+ i +']']}));
  }
  console.log("sched: ");
  console.log(sched);
/*
  var newsched = new userModel({
    schedule: sched,
    comment: req.body.comment
  });

  newsched.save(function( err, data)
  {
    if(err) throw err;
    console.log(data);
  });
  */

  userModel.update({_id: req.params.id}, {$set:{schedule: sched}, comments:req.body.comments} , function(err, data) {
    if (err)
      console.log(err);
    res.redirect('/');
    //res.send(data);
  });
  /*
  userModel.update({_id: req.params.id}, {$set:{schedule: sched}} , function(err, data) {
    if (err)
      console.log(err);
    res.redirect('/');
  });
  */
});
module.exports = router;

