var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var hoursAcrossDay = new Schema({
  availability:  ["0","0","0","0","0"]
});

var userSchema = new Schema({
  schedule:[hoursAcrossDay],
  comment: String
  });

module.exports = mongoose.model('user', userSchema);
