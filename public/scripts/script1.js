/**
 * Created by Michelle on 4/27/2016.
 */
$(document).ready(function () {
   $(".sched").click(function() {
       var col = $(this).parent().children().index($(this));
       var row = $(this).parent().parent().children().index($(this).parent());
       console.log('Row: ' + row + ', Column: ' + col);

       if($(this).hasClass("freecell"))
           $(this).removeClass("freecell");
       else
           $(this).addClass("freecell");

       if($(this).children([0]).val() == "0")
           $(this).children([0]).val("1");
       else
           $(this).children([0]).val("0");
       console.log($(this).children([0]).val());
   });
});